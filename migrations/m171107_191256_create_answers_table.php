<?php

use yii\db\Migration;

/**
 * Handles the creation of table `answers`.
 */
class m171107_191256_create_answers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%answers}}', [
            'id' => $this->primaryKey(),
            'body' => $this->text(),
            'question_id' => $this->integer(),
            'author_id' => $this->integer(),
            'rate' => $this->integer(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%answers}}');
    }
}
