<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All questions';
?>
<div class="blog-post">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (!Yii::$app->user->isGuest) { ?>
        <p>
            <?= Html::a('Ask question', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => [
                'class' => 'table table-striped table-hover'
            ],
            'columns' => [
                'id',
                'title',
                'description',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{answer} {view}',
                    'buttons' => [
                        'answer' => function ($url) {
                            if (!Yii::$app->user->isGuest) {
                                return Html::a('<span class="glyphicon glyphicon-education"></span>', $url);
                            }
                        },
                        'view' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                        }

                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = Url::to(['site/view', 'id' => $model->id]);
                            return $url;
                        }

                        if ($action === 'answer') {
                            $url = Url::to(['site/answer', 'id' => $model->id]);
                            return $url;
                        }
                    }


                ],

            ],
        ]); ?>
</div>