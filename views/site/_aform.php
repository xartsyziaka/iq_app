<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\HtmlPurifier;



/* @var $this yii\web\View */
/* @var $model app\models\Answer */
/* @var $question app\models\Question
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
    $model->author_id = Yii::$app->user->id;
}
?>

<div class="blog-post">

    <h2>
        <?= Html::encode($question->title) ?>
    </h2>

    <p>
        <?= HtmlPurifier::process($question->description) ?>
    </p>
    <hr>
    <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal col-md-6',
                'enctype' => 'multipart/form-data',
            ],
        ]

    ); ?>

    <?= $form->field($model, 'author_id')->hiddenInput(['value' => $model->author_id])->label(false) ?>

    <?= $form->field($model, 'body')->textarea()->label('Your answer:')?>

    <?= $form->field($model, 'question_id')->hiddenInput(['value' => $question->id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Reply', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>