<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Question */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
    $model->author_id = Yii::$app->user->id;
}
?>

<div>

    <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal col-md-6',
                'enctype' => 'multipart/form-data',
            ],
        ]
    ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea()?>

    <?= $form->field($model, 'author_id')->hiddenInput(['value' => $model->author_id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>