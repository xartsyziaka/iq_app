<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $question app\models\Question */
/* @var $model app\models\Answer */

$this->title = 'Answer the question';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_aform', [
        'model' => $model,
        'question' => $question
    ]) ?>

</div>