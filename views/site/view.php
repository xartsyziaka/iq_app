<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\HtmlPurifier;
use yii\grid\GridView;
use \chiliec\vote\widgets\Vote;
use \app\models\Answer;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var \app\models\Question $model
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

?>




<div class="blog-post">
    <h2>
        <?= Html::encode($model->title) ?>
    </h2>

    <?= HtmlPurifier::process($model->description) ?>

    <hr>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover answer-rating'
        ],

        'columns' => [
            'body',
            [
                'attribute' => 'rate',
                'value' => function (Answer $answer) {
                    return Vote::widget([
                        'model' => $answer,
                        'showAggregateRating' => false,
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>